import React from "react";
import "./App.css";
import Footer from "./components/footer/Footer";
import Logo from "./assets/tkm.png";

function App() {
  return (
    <div className="App">
      <div className="main">
        <div className="center">
          <img className="mainLogo" src={Logo} alt="logo" width="200" />
          <h1 className="center">The Konekt Media</h1>
          <h4 className="center">A visual storytelling media platform</h4>
          <br />
          <h1 className="center">Comming Soon</h1>
        </div>
        <br />
        <h3 style={{ marginTop: "auto" }}>Follow Us</h3>
      </div>
      <Footer></Footer>
    </div>
  );
}

export default App;
