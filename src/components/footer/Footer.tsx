import React from "react";
import "./footer.css";

export default function Footer() {
  return (
    <div className="footer">
      <div className="content center">
        <div className="center">
          {/* <span>Powered By</span>
          <br /> */}
          <a
            className="powerLogo"
            href="https://twitter.com/TheKonektMedia?s=09"
            target="_blank"
            rel="noopener noreferrer"
          >
            Twitter
          </a>
          <a
            className="powerLogo"
            href="https://www.instagram.com/thekonektmedia"
            target="_blank"
            rel="noopener noreferrer"
          >
            Instagram
          </a>
          <a
            className="powerLogo"
            href="https://m.facebook.com/TheKonektMedia/"
            target="_blank"
            rel="noopener noreferrer"
          >
            Facebook
          </a>
          <a
            className="powerLogo"
            href="https://www.youtube.com/channel/UCuUJqtJvUHwhT5zraHjdnBw"
            target="_blank"
            rel="noopener noreferrer"
          >
            Youtube
          </a>
        </div>
      </div>
      <div className="copyRight">
        <small className="center copyRightContent">
          Copyright &#9400; {new Date().getFullYear()} The Konekt Media. All
          Rights Reserved.
        </small>
      </div>
    </div>
  );
}
